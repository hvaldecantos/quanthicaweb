class UserAlgorithmSubscription < ActiveRecord::Base

  include Approvable

  belongs_to :user
  belongs_to :algorithm

  validates_presence_of :user_id, :algorithm_id
  validates_uniqueness_of :algorithm_id, :scope => :user_id
  
end
