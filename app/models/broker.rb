class Broker < ActiveRecord::Base
  
  validates_uniqueness_of :name
  validates_presence_of :name
  validates_presence_of :market_password_digest

  before_save :encrypt_password, if: :market_password_digest_changed?

  belongs_to :market

  def encrypt_password
    crypt = ActiveSupport::MessageEncryptor.new(Quanthicaweb::Application.config.secret_key_base)
    self.market_password_digest = crypt.encrypt_and_sign(market_password_digest) # unless market_password_digest.blank?
  end

  def decrypt_password_digest
    crypt = ActiveSupport::MessageEncryptor.new(Quanthicaweb::Application.config.secret_key_base)
    crypt.decrypt_and_verify(market_password_digest)
  end

end
