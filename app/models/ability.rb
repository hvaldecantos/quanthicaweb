class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    alias_action :create, :show, :update, :destroy, :to => :crud # Cannot list (index) resources

    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :user
      can :crud, [Profile], :user_id => user.id
      can :manage, [Algorithm], :user_id => user.id
      can :manage, [UserAlgorithmSubscription], :user_id => user.id
      can :manage, [ApiCall], :user_id => user.id
    elsif user.has_role? :guest
      can :crud, [Profile], :user_id => user.id
    end

# can :read, Article, Article.published do |article|
#   article.published_at <= Time.now
# end
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
