class DependentDestroyRestrictionError < Exception
  def initialize(owner, dependent)
    @message = "Cannot destroy #{dependent} while his #{owner} exists."
  end
  def message
    @message
  end
end
