class Profile < ActiveRecord::Base
  belongs_to :user

  validates_presence_of :user_id
  validates_uniqueness_of :user_id
  validate :user_id_must_correspond_to_his_owner

  before_destroy :check_owner_user

  def full_name
    fname = user.email
    if (!name.blank? || !last_name.blank?)
      fname = ""
      fname = name + " " unless name.blank?
      fname = fname + last_name unless last_name.blank?
      fname.strip!
    end
    fname
  end

  private
    def check_owner_user   
      unless user.nil?     
        raise DependentDestroyRestrictionError.new("user", "profile")
      end 
    end

    def user_id_must_correspond_to_his_owner
      if user.nil?
        errors.add(:user_id, "Cannot change user_id profile.")
      end
    end
end
