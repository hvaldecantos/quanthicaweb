module Approvable
  
  # ActiveRecord attributes required on models that include Approvable module:
  # approved: boolean

  extend ActiveSupport::Concern

  def approved!
    update_attributes(:approved => true)
  end

  def disapproved!
    update_attributes(:approved => false)
  end

end
