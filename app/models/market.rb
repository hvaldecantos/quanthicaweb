class Market < ActiveRecord::Base
  validates_uniqueness_of :name

  has_many :brokers
end
