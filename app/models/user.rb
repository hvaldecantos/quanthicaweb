class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :assignments, dependent: :destroy
  has_many :roles, :through => :assignments
  has_many :algorithms, dependent: :destroy
  has_many :api_calls, dependent: :destroy
  has_one :profile
  has_one :api_key, dependent: :destroy

  after_create :create_associated_profile
  after_create :set_guest_role
  after_destroy :destroy_profile

  has_many :user_algorithm_subscriptions, dependent: :destroy
  has_many :subscribed_algorithms, :through => :user_algorithm_subscriptions, :source => :algorithm

  def has_role?(role_name_sym)
    roles.any? { |r| r.name.underscore.to_sym == role_name_sym }
  end

  private
    def create_associated_profile
      create_profile(user_id: id)
    end

    def set_guest_role
      roles << Role.find_by(name: ENV['GUEST_ROLE_NAME']) if roles.empty?
    end

    def destroy_profile
      profile.destroy
    end
    
end
