class Algorithm < ActiveRecord::Base
  
  include Approvable

  belongs_to :user
  scope :all_owned_by, ->(user) { where(:user_id => user.id)}

  has_many :user_algorithm_subscriptions, dependent: :destroy
  has_many :subscribed_users, :through => :user_algorithm_subscriptions, :source => :user

  validates_presence_of :name
  validates_uniqueness_of :name
end
