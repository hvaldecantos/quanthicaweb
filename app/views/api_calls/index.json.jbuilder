json.array!(@api_calls) do |api_call|
  json.extract! api_call, :id, :user_id, :ip, :http_verb, :api_version, :path, :params
  json.url api_call_url(api_call, format: :json)
end
