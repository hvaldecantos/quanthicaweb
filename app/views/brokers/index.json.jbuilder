json.array!(@brokers) do |broker|
  json.extract! broker, :id, :name, :email, :market_username, :market_password_digest, :market_id, :fix_SenderCompID
  json.url broker_url(broker, format: :json)
end
