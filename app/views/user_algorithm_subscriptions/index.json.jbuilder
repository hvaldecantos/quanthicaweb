json.array!(@user_algorithm_subscriptions) do |user_algorithm_subscription|
  json.extract! user_algorithm_subscription, :id, :user_id, :algorithm_id, :approved
  json.url user_algorithm_subscription_url(user_algorithm_subscription, format: :json)
end
