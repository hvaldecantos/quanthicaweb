json.array!(@markets) do |market|
  json.extract! market, :id, :name, :ip, :port, :fix_HeartBtInt, :fix_BeginString, :fix_TargetCompID
  json.url market_url(market, format: :json)
end
