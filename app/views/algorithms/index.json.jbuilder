json.array!(@algorithms) do |algorithm|
  json.extract! algorithm, :id, :user_id, :name, :approved
  json.url algorithm_url(algorithm, format: :json)
end
