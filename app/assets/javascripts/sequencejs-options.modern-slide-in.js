$(document).ready(function(){
    var options = {
        nextButton: false,
        prevButton: false,
        pagination: false,
        animateStartingFrameIn: true,
        autoPlay: true,
        autoPlayDelay: 3000,
		pauseOnHover:false,
        preloader: false,
		cycle:false,
//        preloadTheseFrames: [1],
//        preloadTheseImages: [
//            "images/tn-model1.png",
//            "images/tn-model2.png",
//            "images/tn-model3.png"
//        ]
    };
    
    var mySequence = $("#sequence").sequence(options).data("sequence");
});