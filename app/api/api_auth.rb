require 'grape'

module API
  class Auth < Grape::API
    version 'v1', using: :header, vendor: 'quanthica', format: :json

    resource :auth do
      desc "Creates and returns access token if valid login"
            
      # curl -X POST http://dev.quanthica.com/api/auth/login -d "email=example@quanthica.com" -d "password=12345678"
      post :login do
        params do
          requires :email, type: String, desc: "Email address"
          requires :password, type: String, desc: "Password"
        end

        user = User.find_by_email(params[:email].downcase)

        if user && user.valid_password?(params[:password])
          key = ApiKey.find_by(user_id: user.id)
          key.destroy unless key.nil?
          key = ApiKey.create(user_id: user.id)
          @current_user = user
          {token: key.access_token}
        else
          error!({ error: "Unauthorized", detail: "Wrong password or email", status: '401' }, 401)
        end
      end

      # curl -X POST http://dev.quanthica.com/api/auth/logout -d "token=c3bef93548e050443088d044d3266b1a"
      post :logout do
        params do
          requires :token, type: String, desc: "Token"
        end
        authenticate!
        key = ApiKey.find_by(access_token: params[:token])
        key.destroy
        {detail: 'Succesfully logged out', status: '201'}
      end  
    end
  end
end
