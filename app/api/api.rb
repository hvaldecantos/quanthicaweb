require 'grape'
require 'api_exceptions.rb'
require 'api_auth.rb'
require 'api_v1.rb'

module API
  class Base < Grape::API

    format :json

    before do
      if(request.path != '/api/status' && ENV['API_STATUS_CODE'] == '503')
        error!({ error: "Service Unavailable", detail: ENV['API_STATUS_MESSAGE'], status: ENV['API_STATUS_CODE'] }, 503)
      end
    end

    after do
      params[:password] = "********" if(params.has_key? :password)
      ApiCall.create(user_id: (current_user.id unless (current_user.nil? || current_user==false)),
                     ip: request.ip,
                     http_verb: params[:route_info].route_method,
                     api_version: params[:route_info].route_version,
                     path: request.path,
                     params: params.except(:route_info).except(:token).to_json)
    end

    rescue_from CanCan::AccessDenied do |e|
      Rack::Response.new({ error: "Forbidden", detail: "Access denied", status: '403' }.to_json, 403).finish
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      Rack::Response.new({ error: "Not Found", detail: e.message, status: '404' }.to_json, 404).finish
    end

    helpers do
      def authenticate!
        error!({ error: "Unauthorized", detail: "Invalid or expired token", status: '401' }, 401) unless current_user
      end

      def authorize!(action, subject, *extra_args)
        if(subject.is_a? ActiveRecord::Relation)
          subject.accessible_by(current_ability, action)
        else
          current_ability.authorize!(action, subject, *extra_args)
        end
      end

      def current_user
        @current_user || get_current_user
      end

      def current_ability
        @current_ability || get_current_ability
      end
      
      def  get_current_user
        token = ApiKey.where(access_token: params[:token]).first
        if token && !token.expired?
          @current_user = User.find(token.user_id)
        else
          false
        end
      end

      def get_current_ability
        @current_ability = Ability.new(current_user)
      end
    end

    mount API::Auth
    mount API::V1
    mount API::Exceptions

  end
end
