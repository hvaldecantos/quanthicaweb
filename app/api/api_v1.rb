require 'grape'

module API
  class V1 < Grape::API
    version 'v1', using: :header, vendor: 'quanthica'
    format :json

    # curl -X GET http://dev.quanthica.com/api/status
    desc "API status returns the status detail and code of the API"
    get :status do
      { detail: ENV['API_STATUS_MESSAGE'], status: ENV['API_STATUS_CODE'] }
    end

    # curl -X GET http://dev.quanthica.com/api/whoami?token=740209e706668437c89be2be6525801c
    desc "Who am I? returns last_name and name of the requester"
    get :whoami do
      params do
        requires :token, type: String, desc: "Access token."
      end
      authenticate!
      user = current_user
      { :last_name => user.profile.last_name.to_s, 
        :name => user.profile.name.to_s
      }
    end

    resource :algorithms do
      params do
        requires :token, type: String, desc: "Access token."
      end

      # curl -X GET http://dev.quanthica.com/api/algorithms?token=740209e706668437c89be2be6525801c
      desc "Return all algorithms owned by the requester (all algorithms in db if admin role)."
      get do
        authenticate!
        authorize! :read, Algorithm.all
      end

      # curl -X GET http://dev.quanthica.com/api/algorithms/:id?token=740209e706668437c89be2be6525801c
      desc "Return an specific algorithm."
      route_param :id do
        get do
          authenticate!
          authorize! :read, Algorithm.find(params[:id])
        end
      end
    end
  
  end
end
