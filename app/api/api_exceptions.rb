require 'grape'

module API
  class Exceptions < Grape::API
    
    route :any, '*path' do 
      error!({ error:  'Not Found',
               detail: "No such route '#{request.path}'",
               status: '404' },
             404)
    end

    route :any do
      error!({ error:  'Not Found',
               detail: "No such route '#{request.path}'",
               status: '404' },
             404)
    end

  end
end
