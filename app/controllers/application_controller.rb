class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :authenticate_user!, :except => ['home', 'about', 'register_email']

  rescue_from DependentDestroyRestrictionError do |exception|
    respond_to do |format|
      format.html { redirect_to :back, :alert => exception.message }
      format.json { render json: "Forbidden", status: :forbidden } # -> Ver que error va!!
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.html { redirect_to root_url, :alert => exception.message }
      format.json { render json: "Forbidden", status: :forbidden }
    end
  end

  # Fix cancan ForbiddenError with rails 4 (generate params with namespace_resource)
  before_filter do
    resource = controller_path.singularize.gsub('/', '_').to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  before_filter :check_if_can_approve_resource, only: [:create, :update]

  protected
    def check_if_can_approve_resource
      begin
        controller_name.classify.constantize
      rescue
        return
      end

      if controller_name.classify.constantize.included_modules.include?(Approvable)
        if !current_user.has_role?(:admin)
          old_value = controller_name.classify.constantize.find(params[:id]).approved unless (action_name == "create")
          new_value = (params[controller_name.classify.underscore.to_sym][:approved] == "1")    
          if (action_name == "create" && new_value == true) || (old_value != new_value)
            raise CanCan::AccessDenied.new("You are not authorized to approve this resource", :update, controller_name.classify.constantize)
          end
        end
      end
    end

end
