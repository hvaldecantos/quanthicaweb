class ApiCallsController < ApplicationController

  load_and_authorize_resource
  before_action :set_api_call, only: [:show, :destroy]

  # GET /api_calls
  # GET /api_calls.json
  def index
    ApiCall.all
  end

  # GET /api_calls/1
  # GET /api_calls/1.json
  def show
  end

  # POST /api_calls
  # POST /api_calls.json
  def create
    @api_call = ApiCall.new(api_call_params)

    respond_to do |format|
      if @api_call.save
        format.html { redirect_to @api_call, notice: 'Api call was successfully created.' }
        format.json { render action: 'show', status: :created, location: @api_call }
      else
        format.html { render action: 'new' }
        format.json { render json: @api_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api_calls/1
  # DELETE /api_calls/1.json
  def destroy
    @api_call.destroy
    respond_to do |format|
      format.html { redirect_to api_calls_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_call
      @api_call = ApiCall.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def api_call_params
      params.require(:api_call).permit(:user_id, :ip, :http_verb, :api_version, :path, :params)
    end
end
