class UserAlgorithmSubscriptionsController < ApplicationController

  load_and_authorize_resource
  before_action :set_user_algorithm_subscription, only: [:show, :edit, :update, :destroy]

  # GET /user_algorithm_subscriptions
  # GET /user_algorithm_subscriptions.json
  def index
    UserAlgorithmSubscription.all
  end

  # GET /user_algorithm_subscriptions/1
  # GET /user_algorithm_subscriptions/1.json
  def show
  end

  # GET /user_algorithm_subscriptions/new
  def new
    @user_algorithm_subscription = UserAlgorithmSubscription.new
  end

  # GET /user_algorithm_subscriptions/1/edit
  def edit
  end

  # POST /user_algorithm_subscriptions
  # POST /user_algorithm_subscriptions.json
  def create
    @user_algorithm_subscription = UserAlgorithmSubscription.new(user_algorithm_subscription_params)

    respond_to do |format|
      if @user_algorithm_subscription.save
        format.html { redirect_to @user_algorithm_subscription, notice: 'User algorithm subscription was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user_algorithm_subscription }
      else
        format.html { render action: 'new' }
        format.json { render json: @user_algorithm_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_algorithm_subscriptions/1
  # PATCH/PUT /user_algorithm_subscriptions/1.json
  def update
    respond_to do |format|
      if @user_algorithm_subscription.update(user_algorithm_subscription_params)
        format.html { redirect_to @user_algorithm_subscription, notice: 'User algorithm subscription was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user_algorithm_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_algorithm_subscriptions/1
  # DELETE /user_algorithm_subscriptions/1.json
  def destroy
    @user_algorithm_subscription.destroy
    respond_to do |format|
      format.html { redirect_to user_algorithm_subscriptions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_algorithm_subscription
      @user_algorithm_subscription = UserAlgorithmSubscription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_algorithm_subscription_params
      params.require(:user_algorithm_subscription).permit(:user_id, :algorithm_id, :approved)
    end
end
