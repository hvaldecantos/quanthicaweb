class StaticPagesController < ApplicationController
  
  def home
  end

  def about
  end

  def register_email
    email_address = params[:correo]
    if is_valid_email email_address then
      save_email_addres_to_csv_file email_address
      render 'email_registration_thanks'
    else
      flash[:alert] = "#{email_address} no parece ser un email válido"
      redirect_to root_url
    end
  end

  private
    def is_valid_email email
      ((/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i =~ email) != nil)
    end

    def save_email_addres_to_csv_file email
      require 'csv'

      directory_name = Rails.root.join('private')
      Dir.mkdir directory_name unless Dir.exists?(directory_name)

      file_name = Rails.root.join('private', 'email_addresses.csv')
      CSV.open(file_name, 'a'){|row| row << ['[email_addresses]']} unless File.exist?(file_name) 
      if CSV.read(file_name).find{ |row| row == [email] }.nil? then
        CSV.open(file_name, 'a'){ |row| row << [email] }
      end
    end
end
