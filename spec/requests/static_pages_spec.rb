require 'spec_helper'

describe "Static pages" do

  describe "Home page" do
    it "should have the content 'Quanthica trading algorithms website'" do
      visit root_path
      expect(page).to have_content('Quanthica trading algorithms website')
    end
    it "should have the right title" do
      visit root_path
      expect(page).to have_title("Quanthica | Home")
    end
  end

  describe "About page" do
    it "should have the content 'About Us'" do
      visit about_path
      expect(page).to have_content('About Us')
    end
    it "should have the right title" do
      visit about_path
      expect(page).to have_title("Quanthica | About")
    end
  end
end