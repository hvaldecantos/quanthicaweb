require "spec_helper"

describe UserAlgorithmSubscriptionsController do
  describe "routing" do

    it "routes to #index" do
      get("/user_algorithm_subscriptions").should route_to("user_algorithm_subscriptions#index")
    end

    it "routes to #new" do
      get("/user_algorithm_subscriptions/new").should route_to("user_algorithm_subscriptions#new")
    end

    it "routes to #show" do
      get("/user_algorithm_subscriptions/1").should route_to("user_algorithm_subscriptions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/user_algorithm_subscriptions/1/edit").should route_to("user_algorithm_subscriptions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/user_algorithm_subscriptions").should route_to("user_algorithm_subscriptions#create")
    end

    it "routes to #update" do
      put("/user_algorithm_subscriptions/1").should route_to("user_algorithm_subscriptions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/user_algorithm_subscriptions/1").should route_to("user_algorithm_subscriptions#destroy", :id => "1")
    end

  end
end
