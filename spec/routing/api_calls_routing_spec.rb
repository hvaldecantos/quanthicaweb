require "spec_helper"

describe ApiCallsController do
  describe "routing" do

    it "routes to #index" do
      get("/api_calls").should route_to("api_calls#index")
    end

    it "routes to #new" do
      get("/api_calls/new").should route_to("api_calls#new")
    end

    it "routes to #show" do
      get("/api_calls/1").should route_to("api_calls#show", :id => "1")
    end

    it "routes to #edit" do
      get("/api_calls/1/edit").should route_to("api_calls#edit", :id => "1")
    end

    it "routes to #create" do
      post("/api_calls").should route_to("api_calls#create")
    end

    it "routes to #update" do
      put("/api_calls/1").should route_to("api_calls#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/api_calls/1").should route_to("api_calls#destroy", :id => "1")
    end

  end
end
