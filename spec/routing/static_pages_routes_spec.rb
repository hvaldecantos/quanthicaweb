require "spec_helper"

describe "Routes for static pages" do
  describe "When named routes are translated to a controller action" do
    it "should translate root_path to static_pages#home" do
      expect(:get => root_path).to route_to(:controller => "static_pages", :action => "home")
    end
    it "should translate about_path to static_pages#about" do
      expect(:get => about_path).to route_to(:controller => "static_pages", :action => "about")
    end
  end

  describe "When named routes are translated to the right uri" do
    it 'should translate root_path to / URI' do
      root_path.should == '/'
    end
    it 'should translate about_path to /about URI' do
      about_path.should == '/about'
    end
  end
end
