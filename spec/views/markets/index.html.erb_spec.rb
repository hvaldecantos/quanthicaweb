require 'spec_helper'

describe "markets/index" do
  before(:each) do
    assign(:markets, [
      stub_model(Market,
        :name => "Name",
        :ip => "Ip",
        :port => 1,
        :fix_HeartBtInt => 2,
        :fix_BeginString => "Fix Begin String",
        :fix_TargetCompID => "Fix Target Comp"
      ),
      stub_model(Market,
        :name => "Name",
        :ip => "Ip",
        :port => 1,
        :fix_HeartBtInt => 2,
        :fix_BeginString => "Fix Begin String",
        :fix_TargetCompID => "Fix Target Comp"
      )
    ])
  end

  it "renders a list of markets" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Ip".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Fix Begin String".to_s, :count => 2
    assert_select "tr>td", :text => "Fix Target Comp".to_s, :count => 2
  end
end
