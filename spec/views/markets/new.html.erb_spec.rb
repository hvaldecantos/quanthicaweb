require 'spec_helper'

describe "markets/new" do
  before(:each) do
    assign(:market, stub_model(Market,
      :name => "MyString",
      :ip => "MyString",
      :port => 1,
      :fix_HeartBtInt => 1,
      :fix_BeginString => "MyString",
      :fix_TargetCompID => "MyString"
    ).as_new_record)
  end

  it "renders new market form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", markets_path, "post" do
      assert_select "input#market_name[name=?]", "market[name]"
      assert_select "input#market_ip[name=?]", "market[ip]"
      assert_select "input#market_port[name=?]", "market[port]"
      assert_select "input#market_fix_HeartBtInt[name=?]", "market[fix_HeartBtInt]"
      assert_select "input#market_fix_BeginString[name=?]", "market[fix_BeginString]"
      assert_select "input#market_fix_TargetCompID[name=?]", "market[fix_TargetCompID]"
    end
  end
end
