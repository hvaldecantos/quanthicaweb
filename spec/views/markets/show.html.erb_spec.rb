require 'spec_helper'

describe "markets/show" do
  before(:each) do
    @market = assign(:market, stub_model(Market,
      :name => "Name",
      :ip => "Ip",
      :port => 1,
      :fix_HeartBtInt => 2,
      :fix_BeginString => "Fix Begin String",
      :fix_TargetCompID => "Fix Target Comp"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Ip/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/Fix Begin String/)
    rendered.should match(/Fix Target Comp/)
  end
end
