require 'spec_helper'

describe "user_algorithm_subscriptions/show" do
  before(:each) do
    @user_algorithm_subscription = assign(:user_algorithm_subscription, stub_model(UserAlgorithmSubscription,
      :user_id => 1,
      :algorithm_id => 2,
      :approved => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/false/)
  end
end
