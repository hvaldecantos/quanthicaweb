require 'spec_helper'

describe "user_algorithm_subscriptions/index" do
  before(:each) do
    assign(:user_algorithm_subscriptions, [
      stub_model(UserAlgorithmSubscription,
        :user_id => 1,
        :algorithm_id => 2,
        :approved => false
      ),
      stub_model(UserAlgorithmSubscription,
        :user_id => 1,
        :algorithm_id => 2,
        :approved => false
      )
    ])
  end

  it "renders a list of user_algorithm_subscriptions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
