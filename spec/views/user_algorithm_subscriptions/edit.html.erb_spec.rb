require 'spec_helper'

describe "user_algorithm_subscriptions/edit" do
  before(:each) do
    @user_algorithm_subscription = assign(:user_algorithm_subscription, stub_model(UserAlgorithmSubscription,
      :user_id => 1,
      :algorithm_id => 1,
      :approved => false
    ))
  end

  it "renders the edit user_algorithm_subscription form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", user_algorithm_subscription_path(@user_algorithm_subscription), "post" do
      assert_select "input#user_algorithm_subscription_user_id[name=?]", "user_algorithm_subscription[user_id]"
      assert_select "input#user_algorithm_subscription_algorithm_id[name=?]", "user_algorithm_subscription[algorithm_id]"
      assert_select "input#user_algorithm_subscription_approved[name=?]", "user_algorithm_subscription[approved]"
    end
  end
end
