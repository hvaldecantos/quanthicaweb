require 'spec_helper'

describe "brokers/edit" do
  before(:each) do
    @broker = assign(:broker, stub_model(Broker,
      :name => "MyString",
      :email => "MyString",
      :market_username => "MyString",
      :market_password_digest => "MyString",
      :market_id => 1,
      :fix_SenderCompID => "MyString"
    ))
  end

  it "renders the edit broker form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", broker_path(@broker), "post" do
      assert_select "input#broker_name[name=?]", "broker[name]"
      assert_select "input#broker_email[name=?]", "broker[email]"
      assert_select "input#broker_market_username[name=?]", "broker[market_username]"
      assert_select "input#broker_market_password_digest[name=?]", "broker[market_password_digest]"
      assert_select "input#broker_market_id[name=?]", "broker[market_id]"
      assert_select "input#broker_fix_SenderCompID[name=?]", "broker[fix_SenderCompID]"
    end
  end
end
