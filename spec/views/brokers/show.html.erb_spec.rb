require 'spec_helper'

describe "brokers/show" do
  before(:each) do
    @broker = assign(:broker, stub_model(Broker,
      :name => "Name",
      :email => "Email",
      :market_username => "Market Username",
      :market_password_digest => "Market Password Digest",
      :market_id => 1,
      :fix_SenderCompID => "Fix Sender Comp"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Market Username/)
    rendered.should match(/Market Password Digest/)
    rendered.should match(/1/)
    rendered.should match(/Fix Sender Comp/)
  end
end
