require 'spec_helper'

describe "brokers/index" do
  before(:each) do
    assign(:brokers, [
      stub_model(Broker,
        :name => "Name",
        :email => "Email",
        :market_username => "Market Username",
        :market_password_digest => "Market Password Digest",
        :market_id => 1,
        :fix_SenderCompID => "Fix Sender Comp"
      ),
      stub_model(Broker,
        :name => "Name",
        :email => "Email",
        :market_username => "Market Username",
        :market_password_digest => "Market Password Digest",
        :market_id => 1,
        :fix_SenderCompID => "Fix Sender Comp"
      )
    ])
  end

  it "renders a list of brokers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Market Username".to_s, :count => 2
    assert_select "tr>td", :text => "Market Password Digest".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Fix Sender Comp".to_s, :count => 2
  end
end
