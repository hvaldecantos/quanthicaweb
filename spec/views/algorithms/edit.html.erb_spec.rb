require 'spec_helper'

describe "algorithms/edit" do
  before(:each) do
    @algorithm = assign(:algorithm, stub_model(Algorithm,
      :user_id => 1,
      :name => "MyString"
    ))
  end

  it "renders the edit algorithm form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", algorithm_path(@algorithm), "post" do
      assert_select "input#algorithm_user_id[name=?]", "algorithm[user_id]"
      assert_select "input#algorithm_name[name=?]", "algorithm[name]"
    end
  end
end
