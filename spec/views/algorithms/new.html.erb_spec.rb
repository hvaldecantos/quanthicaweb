require 'spec_helper'

describe "algorithms/new" do
  before(:each) do
    assign(:algorithm, stub_model(Algorithm,
      :user_id => 1,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new algorithm form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", algorithms_path, "post" do
      assert_select "input#algorithm_user_id[name=?]", "algorithm[user_id]"
      assert_select "input#algorithm_name[name=?]", "algorithm[name]"
    end
  end
end
