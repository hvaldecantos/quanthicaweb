require 'spec_helper'

describe "api_calls/show" do
  before(:each) do
    @api_call = assign(:api_call, stub_model(ApiCall,
      :user_id => 1,
      :ip => "Ip",
      :http_verb => "Http Verb",
      :api_version => "Api Version",
      :path => "Path",
      :params => "Params"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Ip/)
    rendered.should match(/Http Verb/)
    rendered.should match(/Api Version/)
    rendered.should match(/Path/)
    rendered.should match(/Params/)
  end
end
