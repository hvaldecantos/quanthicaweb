require 'spec_helper'

describe "api_calls/new" do
  before(:each) do
    assign(:api_call, stub_model(ApiCall,
      :user_id => 1,
      :ip => "MyString",
      :http_verb => "MyString",
      :api_version => "MyString",
      :path => "MyString",
      :params => "MyString"
    ).as_new_record)
  end

  it "renders new api_call form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", api_calls_path, "post" do
      assert_select "input#api_call_user_id[name=?]", "api_call[user_id]"
      assert_select "input#api_call_ip[name=?]", "api_call[ip]"
      assert_select "input#api_call_http_verb[name=?]", "api_call[http_verb]"
      assert_select "input#api_call_api_version[name=?]", "api_call[api_version]"
      assert_select "input#api_call_path[name=?]", "api_call[path]"
      assert_select "input#api_call_params[name=?]", "api_call[params]"
    end
  end
end
