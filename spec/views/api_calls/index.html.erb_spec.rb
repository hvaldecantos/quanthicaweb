require 'spec_helper'

describe "api_calls/index" do
  before(:each) do
    assign(:api_calls, [
      stub_model(ApiCall,
        :user_id => 1,
        :ip => "Ip",
        :http_verb => "Http Verb",
        :api_version => "Api Version",
        :path => "Path",
        :params => "Params"
      ),
      stub_model(ApiCall,
        :user_id => 1,
        :ip => "Ip",
        :http_verb => "Http Verb",
        :api_version => "Api Version",
        :path => "Path",
        :params => "Params"
      )
    ])
  end

  it "renders a list of api_calls" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Ip".to_s, :count => 2
    assert_select "tr>td", :text => "Http Verb".to_s, :count => 2
    assert_select "tr>td", :text => "Api Version".to_s, :count => 2
    assert_select "tr>td", :text => "Path".to_s, :count => 2
    assert_select "tr>td", :text => "Params".to_s, :count => 2
  end
end
