# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

puts
puts 'Creating Guest Role...'
role = Role.find_by name: ENV['GUEST_ROLE_NAME']
if role.nil? then role = Role.create name: ENV['GUEST_ROLE_NAME'],
                              description: ENV['GUEST_ROLE_DESCRIPTION']
else puts "Guest role already exists!" end
puts 'Role: ' << role.name

puts
puts 'Creating User Role...'
role = Role.find_by name: ENV['USER_ROLE_NAME']
if role.nil? then role = Role.create name: ENV['USER_ROLE_NAME'],
                              description: ENV['USER_ROLE_DESCRIPTION']
else puts "User role already exists!" end
puts 'Role: ' << role.name

puts
puts 'Creating Admin Role...'
admin_role = Role.find_by name: ENV['ADMIN_ROLE_NAME']
if admin_role.nil? then admin_role = Role.create name: ENV['ADMIN_ROLE_NAME'],
                              description: ENV['ADMIN_ROLE_DESCRIPTION']
else puts "Admin role already exists!" end
puts 'Role: ' << admin_role.name

puts
puts 'Creating Admin User...'
user = User.find_by email: ENV['ADMIN_USER_EMAIL']
if user.nil? then user = User.create email: ENV['ADMIN_USER_EMAIL'], 
                                  password: ENV['ADMIN_USER_PASSWORD'], 
                     password_confirmation: ENV['ADMIN_USER_PASSWORD']
else puts 'Admin user already exists!' end
puts 'User email: ' << user.email

puts
puts "Creating Assignment..."
assignment = Assignment.find_by user_id: user.id, role_id: admin_role.id
if assignment.nil? then assignment = Assignment.create user_id: user.id,
                                                       role_id: admin_role.id
else puts "Assignment for admin user already exists!" end
puts "Role Assignment: [#{assignment.user.email} is #{assignment.role.name}]"

puts
puts 'Creating Rofex Market...'
market = Market.find_by name: "ROFEX"
if market.nil? then market = Market.create name: ENV['ROFEX_MARKET_NAME'],
                                             ip: ENV['ROFEX_MARKET_IP'],
                                           port: ENV['ROFEX_MARKET_PORT'],
                                 fix_HeartBtInt: ENV['ROFEX_MARKET_FIX_HEARTBEAT'],
                                fix_BeginString: ENV['ROFEX_MARKET_FIX_BEGINSTRING'],
                               fix_TargetCompID: ENV['ROFEX_MARKET_FIX_TARGETCOMPID']
else puts "Market Rofex already exists!" end
puts 'Market: ' << market.name
