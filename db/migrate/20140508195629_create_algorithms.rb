class CreateAlgorithms < ActiveRecord::Migration
  def change
    create_table :algorithms do |t|
      t.integer :user_id
      t.string :name

      t.timestamps
    end
  end
end
