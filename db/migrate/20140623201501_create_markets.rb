class CreateMarkets < ActiveRecord::Migration
  def change
    create_table :markets do |t|
      t.string :name
      t.string :ip
      t.integer :port
      t.integer :fix_HeartBtInt
      t.string :fix_BeginString
      t.string :fix_TargetCompID

      t.timestamps
    end
  end
end
