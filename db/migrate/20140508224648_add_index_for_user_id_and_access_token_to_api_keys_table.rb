class AddIndexForUserIdAndAccessTokenToApiKeysTable < ActiveRecord::Migration
  def change
    add_index :api_keys, ["user_id"], name: "index_api_keys_on_user_id", unique: true
    add_index :api_keys, ["access_token"], name: "index_api_keys_on_access_token", unique: true
  end
end
