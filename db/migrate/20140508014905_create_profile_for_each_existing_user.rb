class CreateProfileForEachExistingUser < ActiveRecord::Migration
  def up
    execute "INSERT INTO profiles(user_id, created_at, updated_at) 
             SELECT id as user_id, created_at, updated_at FROM users ORDER BY id;"
  end

  def down
    execute "DELETE FROM profiles"
    execute "ALTER SEQUENCE profiles_id_seq RESTART WITH 1"
  end
end
