class CreateBrokers < ActiveRecord::Migration
  def change
    create_table :brokers do |t|
      t.string :name
      t.string :email
      t.string :market_username
      t.string :market_password_digest
      t.integer :market_id
      t.string :fix_SenderCompID

      t.timestamps
    end
  end
end
