class ChangeDefaultValueToApprovedColumnInUserAlgorithmSubscriptions < ActiveRecord::Migration
  def change
    change_column :user_algorithm_subscriptions, :approved, :boolean, default: false
  end
end
