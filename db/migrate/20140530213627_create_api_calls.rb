class CreateApiCalls < ActiveRecord::Migration
  def change
    create_table :api_calls do |t|
      t.integer :user_id
      t.string :ip
      t.string :http_verb
      t.string :api_version
      t.string :path
      t.string :params

      t.timestamps
    end
  end
end
