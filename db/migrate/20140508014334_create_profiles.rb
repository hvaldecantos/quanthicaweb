class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :last_name
      t.string :name

      t.timestamps
    end
  end
end
