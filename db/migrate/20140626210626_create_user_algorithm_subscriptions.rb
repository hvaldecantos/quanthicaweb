class CreateUserAlgorithmSubscriptions < ActiveRecord::Migration
  def change
    create_table :user_algorithm_subscriptions do |t|
      t.integer :user_id
      t.integer :algorithm_id
      t.boolean :approved

      t.timestamps
    end
  end
end
