class AddColumnApprovedToAlgorithms < ActiveRecord::Migration
  def change
    add_column :algorithms, :approved, :boolean, default: false
  end
end
