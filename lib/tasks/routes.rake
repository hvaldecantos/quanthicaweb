namespace :api do
  desc "API Routes"
  task :routes => :environment do
    puts "#{"Method".rjust(15)}    #{"URI".ljust(30)} #{"Header param".ljust(20)}"
    API::Base.routes.each do |api|
      method = api.route_method || ""
      path = api.route_path
      version = api.route_version || ""
      uri_pattern = "/api#{path}"
      header_param = ("application/vnd.quanthica-#{version}+json" unless version.empty?) || ""
      puts "#{method.rjust(15)}    #{uri_pattern.ljust(30)} #{header_param.ljust(20)}"
    end
    puts
  end
end
